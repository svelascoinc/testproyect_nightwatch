# TestProyect_Nightwatch


Nightwatch Automations Tests

## Installation

- npm install (The necessary dependencies are in the package.json)

## Execution Tests

- nightwatch --testcases

This command will execute all the tests found in the src folder specified in the config file

## Execution Test in other browser

- nightwatch --testcases **-e firefox,safari,etc**

## Execution Tests in Safari


To run the tests it is necessary to enable the option from the console or from the browser

[More Info](https://developer.apple.com/documentation/webkit/testing_with_webdriver_in_safari)


### Observations
Currently the execution is configured to work with Chrome by default

