module.exports = {
    "Step 1 - Checking Page" : function (browser) {
    browser.maximizeWindow()
    browser
    .url("http://automationpractice.com/index.php")
    .waitForElementVisible('body', 2000, abortOnFailure=true, 'Body Loaded')
    .assert.titleContains('My Store')
    .saveScreenshot('./captures/fileName1.png');
    },

    "Step 2 - Home Search: Summer" : function (browser) {
        browser.expect.element('#search_query_top').to.be.present.after(1000);
        browser
        .getLogTypes(function() {
            console.log('Clear values input search')
          })
        .clearValue('#search_query_top')
        browser.expect.element('#search_query_top').text.to.equal('')
        browser
            .setValue('#search_query_top', 'summer')
            .assert.valueContains('#search_query_top', 'summer')
            .click('button[name=submit_search]')
            .saveScreenshot('./captures/fileName2.png');
        },

    "Step 3 - Seach Summer Check" : function (browser) {
        browser
        .assert.titleContains('Search - My Store')
        .assert.urlContains('summer', 'URL contains the expected value : "summer"')
        .assert.valueContains('#search_query_top', 'summer')
        .saveScreenshot('./captures/fileName3.png');
    },
    "Step 4 - Search Results": function(browser) {
  
            browser
            .elements('xpath', '//div[@class="product-container"]',function(result){
                console.log(result.value.length + ' results have been found')
             })
             .saveScreenshot('./captures/fileName4.png');
        
             browser.end();
         }


   };