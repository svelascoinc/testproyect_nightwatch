module.exports = {
  '@tags': ['test'],
    "Step 1 - Load Page" : function (browser) {
    browser.maximizeWindow()
    browser
    .url("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_alert")
    .waitForElementVisible('body', 2000, abortOnFailure=true, 'Body Loaded')
    .assert.titleContains('Tryit Editor v3.6')
    },

    "Step 2 - Switch to iframe" : async function  (browser) {
        await browser.waitForElementVisible("#iframeResult")
        const frame = await browser.element("css selector", "#iframeResult")
        await browser.frame(frame.value)
        browser
        .click('button')
        browser.getLogTypes(function() {
            console.log('click in alert button')
       }) 
    },
    "Step 3 - Validate Alert" : function (browser) {
        browser
        .getAlertText(function(result){
            console.log('The Alert Content: ' + result.value)
         })
         .getLogTypes(function() {
            console.log('Closing alert')
          })
        .acceptAlert()
        .getLogTypes(function() {
            console.log('Switch to parent frame')
          })
        .frameParent()
        .end();
        }


   };