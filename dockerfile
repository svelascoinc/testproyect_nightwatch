# Use the official image as a parent image.
FROM node:latest

# Set the working directory.
WORKDIR /usr/src/app

RUN apt-get update -q -y && apt-get --yes install libnss3 && apt-get --yes install libgconf-2-4

    # Install chrome
    # Add key
RUN curl -sS -L https://dl.google.com/linux/linux_signing_key.pub | apt-key add -
    # Add repo
RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && apt-get update -q -y && apt-get install -y google-chrome-stable

# Copy the file from your host to your current location.
COPY nightwatch.conf.js package.json ./

ADD tests /usr/src/app/tests

# Run the command inside your image filesystem.
RUN npm install
